new WOW().init();

$(document).ready( function() {

  //Add animation for top jeading underline
  $('.top__heading').addClass('top__heading--active');

  //replace standart select text with custom field
  if ( $('.send-cv__viewport').length ) {
    $('.send-cv__viewport').text($('#career_type option')[0].textContent);
    $('#career_type').change(function () {
      $('.send-cv__viewport').text($("option:selected", this).text());
    });
  }

 //Rotate cogs on scroll
 $(window).scroll(function() {
  var theta = $(window).scrollTop() / 100 % Math.PI;
  $('.cog-forward').css({ transform: 'rotate(' + theta + 'rad)' });
  $('.cog-backward').css({ transform: 'rotate(-' + theta + 'rad)' });
  });

  //Show or hide the animation of the work section
  if ( $(window).width() >= 991 && $("#work").length ) {
    var topOfOthDiv = $("#work").offset().top;
    console.log(topOfOthDiv);
    $(window).scroll(function() {
        if($(window).scrollTop() > topOfOthDiv-400) { //scrolled past the other div?
            $("#containerWrapper").addClass('work__wrapper--active'); //reached the desired point -- show div
        }
    });
  }

  //Mobile navigation
  $('#navOpen').click(function() {
    if ( $(this).hasClass('nav__open--active') ) {
      $(this).removeClass('nav__open--active');
      $('#header').animate({height: '86px'}, 1, function(){
        $('.nav__wrapper').hide();
      });
    } else {
      $(this).addClass('nav__open--active');
      $('#header').animate({height: '250px'}, 1, function(){
        $('.nav__wrapper').addClass('nav__wrapper--mobile').show();
        $('.nav__wrapper--mobile').click(function(e){
          console.log(e.target);
          $('#navOpen').removeClass('nav__open--active');
          $('#header').animate({height: '86px'}, 1, function(){
            $('.nav__wrapper--mobile').hide();
          });
        });
      });
    }
  });

  //Accordion on career page
  if ( $('.accordion').length ) {
    $( ".accordion" ).accordion({
      heightStyle: "content",
      active: false,
      collapsible: true
    });
  }

  //Change upload file text when file uploaded
  $('#upload').change(function(){
    var fileName = $('.upload').val().replace(/^.*[\\\/]/, '');
    $('#uploadBtn').html(fileName);
  });

  //Smooth scroll after click on menu item
  $('.nav__item a, .main-link').click(function(event){
    if ( $(this).parent().hasClass('nav__item--career') ) {
      return 0;
    } else {
      event.preventDefault();
      $('html, body').animate({
          scrollTop: $( $.attr(this, 'href') ).offset().top - 85
      }, 500);
    }
  });

});


//Validate form on the main page
function formValidate(){
  var name = $('.form__name').val();
  var email = $('.form__email').val();
  var number = $('.form__tel').val();
  var telRegex = /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{1})/;
  var emailRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  if ( name.length <= 0 || name === null || name.match(/\s/g) || name.match(/\./g ) ) {
    $('.form__name').addClass('form--error');
    return false;
  } else if ( email.length <=0 || email === null || !emailRegex.test(email) ) {
    $('.form__email').addClass('form--error');
    return false;
  } else if ( !telRegex.test(number) ) {
    $('.form__tel').addClass('form--error');
    return false;
  } else {
    return true;
  }
}

//Send form form index page
$("#formMain").submit(function(e) {
    e.preventDefault();

    var url = "path/to/your/script.php"; // the script where you handle the form input.

    if ( formValidate() === true) {
      $.ajax({
             type: "POST",
             url: url,
             data: $("#idForm").serialize(),
             success: function(data)
             {
                 $('#submit').val('Success!').addClass('submit--success');
             },
             error: function(errorThrown){
               alert(errorThrown);
             }
           });
    } else {
      return false;
    }
});

//Remove form--error class on focus
$('.form__name, .form__email, .form__tel').focus(function(){
  $(this).removeClass('form--error');
});




//Career page validation
if( $('#sendCv').length) {
  $('#sendCv').parsley();
}

//Send form form career page
$("#sendCv").submit(function(e) {

    var url = "path/to/your/script.php"; // the script where you handle the form input.

    $.ajax({
           type: "POST",
           url: url,
           data: $("#idForm").serialize(),
           success: function(data)
           {
               $('#submit').val('Success!').addClass('submit--success');
           },
           error: function(errorThrown){
             alert(errorThrown);
           }
         });

    e.preventDefault();
});
