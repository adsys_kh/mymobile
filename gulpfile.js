var gulp = require('gulp'),
    plumber = require('gulp-plumber'),
    rename = require('gulp-rename');


var postcss = require('gulp-postcss');
var autoprefixer = require('autoprefixer');
var sourcemaps   = require('gulp-sourcemaps');
var cssbeautify = require('gulp-cssbeautify');
var cssnano = require('gulp-cssnano');

var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var htmlmin = require('gulp-html-minifier');
var imagemin = require('gulp-imagemin'),
    cache = require('gulp-cache');
var minifycss = require('gulp-minify-css');
var less = require('gulp-less');
var sass = require('gulp-sass');
var gulpretinasprites = require('gulp-retina-sprites');

gulp.task('images', function(){
  gulp.src('src/img/**/*')
    .pipe(cache(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true })))
    .pipe(gulp.dest('dist/img'));
});

gulp.task('retina', function() {
  gulp.src('dist/img/*.png')
		.pipe(gulpretinasprites())
		.pipe(gulp.dest('dist/img/retina'));
})

gulp.task('htmlminify', function() {
  gulp.src('src/*.html')
    //.pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('./dist'))
});

gulp.task('styles', function(){
  gulp.src(['src/styles/**/main.scss'])
    .pipe(plumber({
      errorHandler: function (error) {
        console.log(error.message);
        this.emit('end');
    }}))
    .pipe(sass())
    //.pipe(sourcemaps.init())
    .pipe(postcss([ autoprefixer({ browsers: ['last 3 versions'] }) ]))
    .pipe(cssnano({
          core: false,
          discardComments: false,
          discardUnused: false,
          mergeRules: true,
          minifyFontValues: true
        }))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('dist/styles/'))
    .pipe(rename({suffix: '.min'}))
    //.pipe(minifycss())
    .pipe(cssnano({
          core: true,
          discardComments: true,
          //discardUnused: true,
          mergeRules: true,
          minifyFontValues: true
        }))
    .pipe(gulp.dest('dist/styles/'))
});

gulp.task('vendorjs', function(){
  return gulp.src(['!src/js/main.js', 'src/js/**/*.js'])
    .pipe(plumber({
      errorHandler: function (error) {
        console.log(error.message);
        this.emit('end');
    }}))
    .pipe(concat('vendors.js'))
    .pipe(gulp.dest('dist/js/'))
    .pipe(rename({suffix: '.min'}))
    .pipe(uglify())
    .pipe(gulp.dest('dist/js/'))
});
gulp.task('mainjs', function() {
  return gulp.src('src/js/main.js')
  //.pipe(uglify())
  .pipe(gulp.dest('dist/js'))
});

gulp.task('default', function(){
  gulp.watch('src/img/**/*', ['images']);
  gulp.watch("src/styles/**/main.scss", ['styles']);
  gulp.watch("src/js/**/*.js", ['vendorjs']);
  gulp.watch("src/js/main.js", ['mainjs']);
  gulp.watch("src/*.html", ['htmlminify']);
});
